package eu.pht.services.mock.service;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import eu.pht.services.mock.util.TrainUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@ComponentScan({ "eu.pht.services.mock" })
@EntityScan("eu.pht.services.mock")
@SpringBootApplication
public class IntegrationTest {

	
	@Test
	public void inputTest()
			throws ClientProtocolException, IOException, NoSuchAlgorithmException {


		String content = TrainUtil.readFileToStr("/Users/jbjares/workspaces/PHTWorkspace/tmp/src/main/resources/input.json");
		CloseableHttpClient client = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost("http://127.0.0.1:8022/PHTIntegrationMockServices/pht/submit/metadata/");

		String json = content;
		StringEntity entity = new StringEntity(json);
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");

		CloseableHttpResponse response = client.execute(httpPost);
		assertEquals(200, response.getStatusLine().getStatusCode());
		client.close();
		System.out.println("END");

//		train = facade.findTrainByInternalId(INTERNAL_ID_TEST);
//		Assert.assertNotNull(train);
	}
	
	
	
}
