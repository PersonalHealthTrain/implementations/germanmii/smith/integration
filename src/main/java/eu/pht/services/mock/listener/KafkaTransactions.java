package eu.pht.services.mock.listener;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

//@Component
public class KafkaTransactions {

    private static final Logger log = LoggerFactory.getLogger(KafkaTransactions.class);

//    @Autowired
    Environment env;

//    @KafkaListener(topics = "${topic.name}", containerFactory = "filterKafkaListenerContainerFactory")
    public void messageListener(String value) throws Exception {

        log.info("Received message");

        log.info("msg: "+value);
        Thread.sleep(2000);

        log.info("Processed message");
    }
    

}