package eu.pht.services.mock.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import eu.pht.services.mock.model.IntegrationOutput;


@Configuration
public class KafkaProducerConfig {
 
	@Autowired
	Environment env;
	
//    @Bean
//    public ProducerFactory<String, String> producerFactory() {
//        Map<String, Object> configProps = new HashMap<>();
//        
//        configProps.put(
//          ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, 
//          env.getProperty("bootstrap.host.port"));
//        configProps.put(
//          ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
//          StringSerializer.class);
//        configProps.put(
//          ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
//          StringSerializer.class);
//        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, env.getProperty("group.id"));
//        configProps.put(
//                ProducerConfig.CLIENT_ID_CONFIG, 
//                env.getProperty("client.id"));
//        return new DefaultKafkaProducerFactory<>(configProps);
//    }
// 
//    @Bean
//    public KafkaTemplate<String, String> kafkaTemplate() {
//        return new KafkaTemplate<>(producerFactory());
//    }
    
///===================
    
    
    @Bean
    public ProducerFactory<String, IntegrationOutput> outputProducerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        
        configProps.put(
          ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, 
          env.getProperty("bootstrap.host.port"));
        configProps.put(
          ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
          StringSerializer.class);
        configProps.put(
          ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
          StringSerializer.class);
        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, env.getProperty("group.id"));
        configProps.put(
                ProducerConfig.CLIENT_ID_CONFIG, 
                env.getProperty("client.id"));
        configProps.put(
          ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
          JsonSerializer.class);
        configProps.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        return new DefaultKafkaProducerFactory<>(configProps);
    }
     
    @Bean
    public KafkaTemplate<String, IntegrationOutput> greetingKafkaTemplate() {
        return new KafkaTemplate<>(outputProducerFactory());
    }
    

}