package eu.pht.services.mock.model;

public class IntegrationWagon {
	
	private String wagonId;

	public String getWagonId() {
		return wagonId;
	}

	public void setWagonId(String wagonId) {
		this.wagonId = wagonId;
	}
	

}
