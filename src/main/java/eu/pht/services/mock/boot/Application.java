package eu.pht.services.mock.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@PropertySource("file:src/main/resources/application.yml")
@PropertySource("file:src/main/resources/phtMessageBroker.properties")
@ComponentScan({ "eu.pht.services.mock" })
@EnableKafka
@EnableTransactionManagement
@SpringBootApplication
public class Application extends SpringBootServletInitializer {


	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
