package eu.pht.services.mock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import eu.pht.services.mock.model.IntegrationInput;
import eu.pht.services.mock.model.IntegrationInputWagon;
import eu.pht.services.mock.model.IntegrationOutput;
import eu.pht.services.mock.model.IntegrationOutputResult;
import eu.pht.services.mock.model.IntegrationOutputWagon;


@EnableAspectJAutoProxy
@RestController
@Service
public class MockServices {

	@Autowired
	private KafkaTemplate<String, IntegrationOutput> kafkaTemplate;
	
	@Autowired
	private Environment env;

	@PostMapping(value = "/pht/submit/metadata/", consumes = MediaType.APPLICATION_JSON_VALUE)
	Boolean postMetadata(@RequestBody IntegrationInput input) throws Exception {
		IntegrationOutput out = getOut(input);
		
		
		//TODO Lukas code here!!!
		

		kafkaTemplate.send(env.getProperty("topic.name"),out);
		return Boolean.TRUE;
	}


  
	
	private IntegrationOutput getOut(IntegrationInput input) {

		IntegrationOutput integrationOutput1 = new IntegrationOutput();
		integrationOutput1.setCorrelationId(input.getCorrelationId());
		integrationOutput1.setTrainId(input.getTrainId());
		integrationOutput1.setPhtUser(input.getPhtUser());
		
		List<IntegrationOutputWagon> integrationOutputWagonList = new ArrayList<IntegrationOutputWagon>();
		for(IntegrationInputWagon intWagon:input.getIntegrationInputWagon()) {
		
			IntegrationOutputWagon integrationOutputWagon = new IntegrationOutputWagon();
			integrationOutputWagon.setWagonId(intWagon.getWagonId());
			List<IntegrationOutputResult> integrationOutputResul = new ArrayList<IntegrationOutputResult>();
			IntegrationOutputResult integrationOutputResult = new IntegrationOutputResult();
			integrationOutputResult.setDescription("count");
			integrationOutputResult.setMimetype("text/double");
			
			Random r = new Random();
			String randomValue = Double.toString(100 + (10000 - 100) * r.nextDouble());
			integrationOutputResult.setRepositoryImageId(UUID.randomUUID().toString());
			integrationOutputResult.setResult(randomValue.getBytes());
			integrationOutputResult.setStatusCode("200");
			integrationOutputResul.add(integrationOutputResult);
			integrationOutputWagon.setIntegrationOutputResult(integrationOutputResul.toArray(new IntegrationOutputResult[integrationOutputResul.size()]));
			integrationOutputWagonList.add(integrationOutputWagon);
		}

		integrationOutput1.setIntegrationOutputWagon(integrationOutputWagonList.toArray(new IntegrationOutputWagon[integrationOutputWagonList.size()]));
		

		return integrationOutput1;
	}

	

}
