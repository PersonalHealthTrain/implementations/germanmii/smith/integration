## Integration mock project
It integrates components developed by Joao and with Lukas ones. More details will be added later on. 

### Integration steps: 

#### 1. Step -1: 
1. Add the following lines to the top of the /etc/hosts
```
167.172.175.112            kafkaserver
```
2. Then following the below instructions: 

```
sudo git clone https://gitlab.com/PersonalHealthTrain/implementations/germanmii/smith/integration.git && cd integration/
sudo docker-compose -f src/main/resources/docker-compose-kafka.yml up -d
sudo mvn install -DskipTests && mvn spring-boot:run

```